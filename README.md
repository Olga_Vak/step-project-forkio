Проект выполнен Вакуленко Ольгой и Фетисовой Татьяной.

Распределение заданий:

Задание №1 - Вакуленко Ольга
(Верстка шапки сайта с верхним меню.
Верстка секции People Are Talking About Fork.);

Задание №2 - Фетисова Татьяна
(Верстка блока Revolutionary Editor. 
Верстка секции Here is what you get.
Верстка секции Fork Subscription Pricing.)

При выполнении задания проекта использованы следующие технологии:
Gulp,
Sass(Scss),
Git,
Методология БЭМ,
Библиотека jQuerry

Проект на GitHub Pages
https://tfetisova.github.io/Step-Project-Forkio/




